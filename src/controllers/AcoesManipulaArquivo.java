package controllers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import model.Filme;

public class AcoesManipulaArquivo {

	// Controller faz relação entre o "banco de dados" e a camada Model.
	// Neste exemplo um arquivo simples é usado para persistir os dados.
	public void salvarArquivo(Filme filme) throws IOException{
		PrintWriter gravarArq = new PrintWriter(new FileWriter("Arquivos/filmes.txt",true));
		 			
			gravarArq.println(filme.getNome());
			gravarArq.println(filme.getGenero());
			gravarArq.println(filme.getAno());
			
			gravarArq.close();		
	}
}
